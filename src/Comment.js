import moment from "moment";
import React from "react";
import "./index.css";
import ReactHtmlParser from "react-html-parser";

export const Comment = props => {
  const html = ReactHtmlParser(props.comment.text);
  const formatedTime = moment
    .unix(props.comment.time)
    .format("hh:mm, MMM Do YYYY.");
  return (
    <div className="comment">
      <div className="commentInfo">
        by {props.comment.by} at {formatedTime}
      </div>
      <div>{html}</div>
      {props.comment.comments
        ? props.comment.comments.map(comment => (
            <Comment key={String(comment.id)} comment={comment} />
          ))
        : null}
    </div>
  );
};
