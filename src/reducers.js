import { ADD_COMMENTS, ADD, HIDDEN_STORIES, CLEAR } from "./actionTypes";
import { combineReducers } from "redux";

const commentPusher = (commentOrStory, comments) => {
  if (commentOrStory.id === comments[0].parent) {
    return Object.assign({}, commentOrStory, { comments });
  }

  if (commentOrStory.id !== comments[0].parent) {
    return Object.assign({}, commentOrStory, {
      comments: commentOrStory.comments.map(comment =>
        commentPusher(comment, comments)
      )
    });
  }
};

const stories = (state = [], action) => {
  switch (action.type) {
    case ADD:
      return [...state, ...action.payload]; // concat?
    case ADD_COMMENTS:
      return state.map(item => commentPusher(item, action.payload));
    case CLEAR:
      return [];
    default:
      return state;
  }
};

const hiddenStories = (state = [], action) => {
  switch (action.type) {
    case HIDDEN_STORIES:
      return action.payload.slice(30, action.payload.length);
    default:
      return state;
  }
};
export const reducer = combineReducers({
  stories,
  hiddenStories
});
