import React from "react";
import { Switch, Route } from "react-router-dom";
import { CommentsView } from "./CommentsView";
import App from "./App";

const Main = () => (
  <Switch>
    <Route exact path="/" component={App} />
    <Route path="/commentsView/:number" component={CommentsView} />
  </Switch>
);

export default Main;
