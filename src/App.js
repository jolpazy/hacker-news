import React from "react";
import { Story } from "./Story";
import { connect } from "react-redux";
import { getStories, clearStories, getAllStories } from "./actions";

export class App extends React.Component {
  componentWillMount = () => {
    if (this.props.state.stories.length < 30) this.refresher();
  };

  clickHandler = stories => () => this.props.getStories(stories);
  refresher = () => {
    this.props.clearStories();
    this.props.getAllStories(this.props.state.allStories);
  };

  render() {
    const stories = this.props.state.stories.map((item, index) => {
      return <Story key={String(item.id)} index={index + 1} story={item} />;
    });

    return (
      <div className="display">
        <div className="header">
          <strong className="hn" onClick={this.refresher}>
            Hacker News{" "}
          </strong>
          Top stories
        </div>

        <div className="stories">
          {stories.length === 0 && "Loading, please wait..."}
          {stories}

          {this.props.state.hiddenStories.length > 0 ? (
            <div
              className="more"
              onClick={this.clickHandler(this.props.state.hiddenStories)}
            >
              More
            </div>
          ) : null}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { state };
};

const mapDispatchToProps = dispatch => {
  return {
    getStories: storiesInLine => dispatch(getStories(storiesInLine)),
    clearStories: () => dispatch(clearStories()),
    getAllStories: () => dispatch(getAllStories())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
