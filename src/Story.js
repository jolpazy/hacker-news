import React from "react";
import "./index.css";
import { Link } from "react-router-dom";

export const Story = props => {
  const { title, url, score, by, id, descendants } = props.story;
  const commentsNum = props.story.kids ? props.story.kids.length : 0;
  return (
    <div className="story">
      <span className="number">{props.index}.</span>

      {url ? (
        <a href={url} target="_blank">
          {title}
        </a>
      ) : (
        <Link className="reactLink" to={`/commentsView/${id}`}>
          {title}
        </Link>
      )}

      <br />
      <span className="info">
        {" by"} {by}, score: {score}
        {commentsNum > 0 && (
          <span>
            {", "}
            <Link className="reactLink" to={`/commentsView/${id}`}>
              comments: {descendants}
            </Link>
          </span>
        )}
      </span>
    </div>
  );
};
