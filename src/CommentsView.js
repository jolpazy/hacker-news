import React from "react";
import "./index.css";
import { Comment } from "./Comment";
import { connect } from "react-redux";
import { getComments, getOneStory } from "./actions";
import { Link } from "react-router-dom";

export class CommentsViewComponent extends React.Component {
  componentWillMount = () => {
    if (this.props.stories.length === 0) {
      this.props.getOneStory(this.props.match.params.number, 10);
    }
    const exactStory = this.props.stories.find(
      item => item.id === parseInt(this.props.match.params.number, 10)
    );
    if (exactStory && exactStory.comments.length === 0) {
      this.props.getComments(exactStory.kids);
    }
  };

  render() {
    const exactStory = this.props.stories.find(
      item => item.id === parseInt(this.props.match.params.number, 10)
    );

    if (!exactStory) return <div className="display">Getting the story...</div>;

    const comments = exactStory.comments.map(item => {
      return <Comment key={String(item.id)} comment={item} />;
    });

    return (
      <div className="display">
        <div className="header">
          <Link className="hn" to={"/"}>
            <strong>Hacker News </strong>
          </Link>
          {"Comments for "}
          {exactStory.url ? (
            <a href={exactStory.url} target="_blank">
              {exactStory.title}
            </a>
          ) : (
            exactStory.title
          )}
        </div>
        <div className="comments">
          {comments.length === 0 && "please wait, comments are incoming..."}
          {comments}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { stories: state.stories };
};

const mapDispatchToProps = dispatch => {
  return {
    getComments: kids => dispatch(getComments(kids)),
    getOneStory: id => dispatch(getOneStory(id))
  };
};

export const CommentsView = connect(mapStateToProps, mapDispatchToProps)(
  CommentsViewComponent
);
