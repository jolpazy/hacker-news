import { ADD_COMMENTS, ADD, HIDDEN_STORIES, CLEAR } from "./actionTypes";
import request from "superagent";
import { compareTime } from "./helpers";

export const addStories = payload => {
  return {
    type: ADD,
    payload
  };
};

export const clearStories = () => {
  return {
    type: CLEAR
  };
};

export const hiddenStories = payload => {
  return {
    type: HIDDEN_STORIES,
    payload
  };
};

const addComments = payload => {
  return {
    type: ADD_COMMENTS,

    payload
  };
};

export const getComments = kids => dispatch => {
  Promise.all(
    kids.map(item =>
      request(
        `https://hacker-news.firebaseio.com/v0/item/${item}.json?print=pretty`
      )
    )
  )
    .then(data =>
      data.map(item => Object.assign({}, item.body, { comments: [] }))
    )
    .then(data => data.sort(compareTime).filter(item => !item.deleted))
    .then(data => {
      if (data.length > 0) {
        dispatch(addComments(data));
        data.forEach(item => {
          if (item.kids) {
            dispatch(getComments(item.kids));
          }
        });
      }
    });
};

export const getOneStory = id => dispatch => {
  request(`https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`)
    .then(data => Object.assign({}, data.body, { comments: [] }))
    .then(data => {
      dispatch(addStories([data])); // wrapped in array bc of using spread operator in reducer
      if (data.kids) {
        dispatch(getComments(data.kids));
      }
    });
};

export const getAllStories = () => dispatch => {
  request("https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty")
    .then(data => data.body)
    .then(data => {
      dispatch(hiddenStories(data));
      dispatch(getStories(data.sort(compareTime)));
    })
    .catch(() => {
      console.log("failed");
    });
};

export const getStories = postsInLine => dispatch => {
  const forNow = postsInLine.slice(0, 30);
  Promise.all(
    forNow.map(item =>
      request(
        `https://hacker-news.firebaseio.com/v0/item/${item}.json?print=pretty`
      )
    )
  )
    .then(data =>
      data.map(item => Object.assign({}, item.body, { comments: [] }))
    )
    .then(data => {
      dispatch(addStories(data));
      dispatch(hiddenStories(postsInLine));
    });
};
